//array literals, string literals, object literals
// console.log("Hello Cami");

//group data in a collection
//access array elements
//get and manipulate the length of array
//access and manipulate an array within an array
//Array is a collection of data
/*Arrays are used to store multiple related data/values in a single variable.
It is created/declared using [] brackets also known as "Array Literals"*/
let hobbies = ["Play video games", "Read a book","Listen to music"];
//Arrays make it easy to manage, manipulate a set of data. Arrays have different methgods/functions that allow us to manage our array.
//Methods are functions associated with an object.
//Because Arrays are actually a special type of "object."
console.log(typeof hobbies);

let grades = [75.4, 98.5, 90.12, 91.50];
const planets = ["Mercury", "Venus", "Mars", "Earth"];

/////////////////////////////////////////
//Arrays as a best practice contain values of the same type.
let arraySample = ["Saitama", "One Punch Man", 25000, true];
console.log(arraySample);
//However, since there are no problems creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or framework.
//sa ibang language magiging problem yung array na ibat't iba ng types ang laman, sa JS oks lang

///////////////////////////////////////////////////////
//Array as a collection of data, has methods to manipulate and manage the array.
//Having values with different data type might interfere or conflict with the methods of an array
//mTask
//create a variable which will store an array of at least 5 of your daily routine or task
//create a variable which will store at least 4 capital cities in the world
let tasks = ["Cooking","Watching", "Practicing","Eating","Cleaning"];
let capitalCities = ["Tokyo", "Berlin", "Madrid","Paris"];

console.log(tasks);
console.log(capitalCities);

let samples = [

	"Sample1",
	"Sample2",
	"Sample3",
	"Sample4"
];

console.log(samples);

///////////////////////////////////////////////////////////
//Each item in an array is called an element.
//Array as a collection of data, as a convention, its name is usually plural.
//We can also add the values of variables as elements in an array
let username1 = "fighter_smith1";
let username2 = "georgeKyle5000";
let username3 = "white_night";

let guildMembers = [username1,username2,username3];

console.log(guildMembers);

// ////////////////////////////////////////////////////////////
//.length property
//The .length property of an array tells about the number of elements in the array.
//It can actually also be set and manipulated
//the .length property of an array is a number type, integer type
console.log(tasks.length);
console.log(capitalCities.length);
//In fact, even strings have a .length property, which tells us the number of characters in a string
//strings are able to use some array methods and properties.
let fullName = "Randy Orton";
console.log(fullName.length);
//spaces are considerd characters

///////////////////////////////////////////////
//We can manipulate the .length property of an array. Being that .length property is a number that tells the total number of elements in an array, we can also delete the last item in an array by manipulating the .length property.
//reassign the value of the .length property by subtracting 1 to its current value:
tasks.length = tasks.length-1;
console.log(tasks.length);
console.log(tasks);
//Nawala na talaga yung last item T.T
//We cannot yet delete a specific item in our array, however in the next sessions, we will learn of an array method which will allow us to do so. We can do it now, manually, but that would require us to create our own algorithm to solve that problem
/*tasks.length = tasks.length+1;
console.log(tasks.length);
console.log(tasks);*/
//We could also decrement the .length property of an array. It will produce the result, that it will delete the last item in the array.
capitalCities.length--;
console.log(capitalCities);
//we can use a function to delete an item in an array
fullName.length = fullName.length-1;
console.log(fullName.length)
console.log(fullName);
/////We can't do the same trick with a string

/////////////////////////////////////////////////
//if we can shorten the array by updating the length property, can we also lengthen or add using the same trick?
//forcibly lengthening
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

//////////////////////////////////////////////////
//Accessing the elements of an array
//accesing array elements is one of the more common tasks we do with an array
//This can be done through the use of array indices
//Array elements are ordered according to index.
//in programming languages, such as JS, arrays start from 0.
console.log(capitalCities);

//if we want to acces a particular item in the array, we can do so with array indices, each item are ordered according to their index. We can locate items in an array via their index. To be able to access an item in an array first identify the name of the array, they add [] square brackets and then add the index number of your item.
//syntax: arrayName[index]
//note:index are number types

console.log(capitalCities[0]);

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];
console.log(lakersLegends[1]);//Shaq
//Magic
console.log(lakersLegends[3]);//Magic


////////////////////////////////////////////
//We can also save or store a particular element in a variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

///////////////////////////////////////
//We can also update or reassign the array elements using their index
lakersLegends[2]="Pau Gasol";
console.log(lakersLegends);

// console.log(currentLaker);


//////////////////////////
//mTask
//update and reassign the last two items in the array with your own fave

let favoriteFoods = [

	"Tonkatsu",
	"Adobo",
	"Hamburger",
	"Sinigang",
	"Pizza"

];
console.log(favoriteFoods);

favoriteFoods[3]="Roasted Chicken";
favoriteFoods[4]="Lasagna";

console.log(favoriteFoods);

///////////////////////////////

//Accessing the last item/element in an array
//How can we access the last item in our favorite foods array?
console.log(favoriteFoods[4]);

//what if we do not know the total number of items in the array?
//What if the array is constantly being added into?

///////////////////////////////////////////////////////////// length-1
//We could consistently access the last item in our array by accessing the index via adding the .length property value minues 1 as the index

//favorite foods [5-1] = favoriteFoods[4]

//the index to the last item is arrayName.length-1 because the index starts at 0
console.log(favoriteFoods[favoriteFoods.length-1]);

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Scalabrine", "LaVine", "Longley", "DeRozen"];
console.log(bullsLegends[bullsLegends.length-1]);

////////////////////////////add items via index//////////////////////
//Add items in an array
//Using our indices we can also add items in our array
let newArr = [];
console.log(newArr);
console.log(newArr[0]);
//undefined, arrray exists pero wla pang laman
newArr[0] = "Cloud Strife";
console.log(newArr);
// newArr[2] = "Doja Cat"
// console.log(newArr);
console.log(newArr[1]);
newArr[1] = "Aerith Gainsborough";
console.log(newArr);
//we can add items in our empty array, because we know the total number of items in our array.
//so that we do not update the items in the array when we're adding instead, we can add items at the end of the array

//the curreny last item of the array is accessed and updated instead.
//newArr[2-1] = newArr[1]
newArr[newArr.length-1] = "Tifa Lockhart";
console.log(newArr);

//newArr[2]
newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

///////Looping Over an Array
//We can loop over an array and iterate all items in the array
//Set the counter as the index and set a condition that as the current index iterated is less than the length of the arrat, we will run the loop
//it is set this way becasue the index starts at 0

//loop over and display all items in the newArr array:


	//so the current counter wil be the index to access the item in the array.

let numArr = [5,12,30,46,40];
//check each item in the array if they are divisible by 5 or not

for(let index = 0; index<numArr.length; index++){

	//loop over every item in the numArr array. The current iteration or the current loop number will be used as index.
	//first loop = index = 0 numArr[index] = numArr[0]
	//2nd loop = index = 1 numArr[index] = numArr[1]
	//...

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5.");
	}else{
		console.log(numArr[index] + " is not divisible by 5.");
	}
}

//Multidimensional Arrays
//Multidimensional arrays are arrays that contain other arrays
//collection within a collection

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];


console.log(chessBoard);


/////////////////accessing items in a multi-dmensional array is a bit different than 1 dimensional arrays
//to access an item within an array, first identify or locate the array where the item is.
console.log(chessBoard[1][5]);
//locate the row then the column
/////mTask
console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);
//console.log(arrayName[y][x]);
//console.log(arrayName[row][column]);
// console.log(chessBoard[5][chessBoard.length-1])


console.log("Original Array:");

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
console.log(users);

users[users.length] = "John Cena";
console.log(users);

console.log(users[2]);
console.log(users[4]);

users[newArr.length-1] = "Tifa Lockhart";
console.log(newArr);



